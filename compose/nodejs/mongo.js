
//Object data modelling library for mongo
const mongoose = require('mongoose');

//Mongo db client library
//const MongoClient  = require('mongodb');

//Express web service library
const express = require('express')

//used to parse the server response from json to object.
const bodyParser = require('body-parser');

//instance of express and port to use for inbound connections.
const app = express()
const port = 3000

//connection string listing the mongo servers. This is an alternative to using a load balancer. THIS SHOULD BE DISCUSSED IN YOUR ASSIGNMENT.
const connectionString = 'mongodb://localmongo1:27017,localmongo2:27017,localmongo3:27017/sweetShopDB?replicaSet=cfgrs';

setInterval(function() {

  console.log(`Intervals are used to fire a function for the lifetime of an application.`);

}, 3000);

//tell express to use the body parser. Note - This function was built into express but then moved to a seperate package.
app.use(bodyParser.json());

//connect to the cluster
mongoose.connect(connectionString, {useNewUrlParser: true, useUnifiedTopology: true});


var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

var Schema = mongoose.Schema;

var infoSchema = new Schema({
  AccountID: Number,
  Username: String,
  TitleID: Number,
  UserAction: String,
  dateTime: String,
  InteractionPoint: String,
  InteractionType: String
});

var infoModel = mongoose.model('Info', infoSchema, 'info');



app.get('/', (req, res) => {
  infoModel.find({},'AccountID Username TitleID UserAction dateTime InteractionPoint InteractionType', (err, info) => {
    if(err) return handleError(err);
    res.send(JSON.stringify(info))
  }) 
})

app.post('/',  (req, res) => {
  var awesome_instance = new infoModel(req.body);
  awesome_instance.save(function (err) {
  if (err) res.send('Error');
    res.send(JSON.stringify(req.body))
  });
})

app.put('/',  (req, res) => {
  res.send('Got a PUT request at /')
})

app.delete('/',  (req, res) => {
  res.send('Got a DELETE request at /')
})

//all code under here was an attempt to create node leaders, however it does not function correctly, the containers still function however, to just have it 
//spam less messages comment out below code.

//bind the express web service to the port specified
app.listen(port, () => {
 console.log(`Express Application listening at port ` + port)
})

systemLeader = 0

//Get the hostname of the node
var os = require("os");
var vmhostname = os.hostname();



var nodeID = Math.floor(Math.random() * (100 - 1 + 1) + 1);
toSend = {"hostname" : vmhostname, "status": "alive","nodeID":nodeID, "Leader":"no"} ;

var amqp = require('amqplib/callback_api');
amqp.connect('amqp://user:bitnami@192.168.56.104', function(error0, connection) {
      if (error0) {
              throw error0;
            }
      connection.createChannel(function(error1, channel) {});
});

var msg =  'Alive';


amqp.connect('amqp://user:bitnami@192.168.56.104', function(error0, connection) {
if (error0) {
        throw error0;
      }
      connection.createChannel(function(error1, channel) {
              if (error1) {
                        throw error1;
                      }
              var exchange = 'logs';
              

              channel.assertExchange(exchange, 'fanout', {
                        durable: false
                      });
              channel.publish(exchange, '', Buffer.from(msg));
              console.log(" [x] Sent %s", msg);
            });





    setTimeout(function() {
              connection.close();
              }, 500);
});


amqp.connect('amqp://user:bitnami@192.168.56.104', function(error0, connection) {
      if (error0) {
              throw error0;
            }
      connection.createChannel(function(error1, channel) {
              if (error1) {
                        throw error1;
                      }
              var exchange = 'logs';

              channel.assertExchange(exchange, 'fanout', {
                        durable: false
                      });

              channel.assertQueue('', {
                        exclusive: true
                      }, function(error2, q) {
                                if (error2) {
                                            throw error2;
                                          }
                                console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", q.queue);
                                channel.bindQueue(q.queue, exchange, '');

                                channel.consume(q.queue, function(msg) {
                                            if(msg.content) {
                                                            console.log(" [x] %s", msg.content.toString());
                                                          }
                                          }, {
                                                      noAck: true
                                                    });
                              });
            });
});

var nodes = {"node1":{"ip":"192.168.56.104"},
"node2":{"ip":"192.168.56.104"},
"node3":{"ip":"192.168.56.104"}};

//check if leader
setInterval(function() {
  leader = 1;
  activeNodes = 0;
  Object.entries(nodes).forEach(([hostname,prop]) => {
    console.log("test" + JSON.stringify(hostname) + JSON.stringify(prop) )
    maxNodeID = nodeID;
    if(hostname != vmhostname){
        activeNodes++;
        if(prop.nodeID > nodeID)
        {
          leader = 0;
        }
      }
    if((leader == 1) && (activeNodes == (nodes.length - 1)))
    {
      systemLeader = 1;
    }
    if (systemLeader == 1) {
      console.log("This node is leader" + vmhostname)
    }
  });
}, 2000);
/*
//for each key value in nodes
Object.entries(nodes).forEach(([hostname,prop]) => {
  
    //print the hostname IP
    console.log("hostname = " + hostname + " ip = " + prop.ip);
//create a number of subscribers to connect to publishers
    var subsockets = [];
    if(vmhostname != hostname ){
        tempsoc = rmq.socket("sub");
        tempsoc.connect("tcp://" + prop.ip + ":3000");
        tempsoc.subscribe("status");
        console.log("Subscriber connected to port 3000 of " + hostname);
        tempsoc.on("message", function(topic, message) {
          jsonMessage = JSON.parse(message.toString("utf-8"));
            console.log(
            "received a message from " + hostname + " related to:",
            topic.toString("utf-8"),
            "containing message:",
            jsonMessage.status + " with ID" + jsonMessage.nodeID
            );
            nodes[hostname].nodeID =  jsonMessage.nodeID;
        });
        //push this instance of a sub socket to the list.
        subsockets.push(tempsoc);
    }
});
*/

